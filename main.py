#!/usr/bin/env python3

__author__ = "Paul Zeinlinger"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse

from space.space import DiscreteSpace
import glob


def main(args):
    """ Main entry point of the app """
    for filename in glob.glob("data/*.dat"):
        print(filename)
        lines = []
        with open(filename, "r") as f:
            lines = f.readlines()
        discreteSpace = DiscreteSpace(lines)
        discreteSpace.update_potential()
        discreteSpace.update_vectors()
        discreteSpace.exportGraphic(
            [filename + "_result.svg", filename + "_result.png"]
        )


if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()

    # Required positional argument
    parser.add_argument("input", help="Required positional argument")

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__),
    )

    args = parser.parse_args()
    main(args)
