from typing import List, Union
from itertools import chain
import re
import matplotlib as plt

plt.use("AGG")
import seaborn
import pandas as pd


class Scalar(object):
    res = 1

    def __init__(self, value=0):
        self.value = value

    def __str__(self):
        return "{: 6.2f}".format(round(self.value, 2))


class Vector(object):
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class BoundaryCondition(Scalar):
    def __str__(self):
        return "{:>5.1f}!".format(round(self.value, 1))


class Point(object):
    def __init__(self, x, y, scalar: Union[Scalar, BoundaryCondition], vector: Vector):
        self.x = x
        self.y = y
        self.scalar = scalar
        self.vector = vector

    def __str__(self):
        return str(self.scalar)


class DiscreteSpace(object):
    def __init__(self, lines=None):
        regex = re.compile(r"(\*?-?\d+)")
        self.xdim = len(lines)
        self.ydim = len(regex.findall(lines[0]))
        print(self.xdim, self.ydim)
        self._grid = [
            [Point(x, y, Scalar(), Vector()) for y in range(self.ydim)]
            for x in range(self.xdim)
        ]
        for x, line in enumerate(lines):
            for y, value in enumerate(regex.findall(line)):
                if value[0] == "*":
                    self._setPoint(x, y, BoundaryCondition(int(value[1:])))

    def column(self, y) -> List[Point]:
        cells = []
        for row in self._grid:
            cells.append(row[y])
        return cells

    def row(self, x) -> List[Point]:
        return self._grid[x]

    def _getPoint(self, x, y) -> Union[Point, None]:
        if x < self.xdim and y < self.ydim and x >= 0 and y >= 0:
            return self._grid[x][y]
        return None

    def _setPoint(self, x, y, scalar):
        self._grid[x][y].scalar = scalar

    @property
    def points(self) -> List[Point]:
        return list(chain.from_iterable([row for row in self._grid]))

    @property
    def scalars(self) -> List[Point]:
        return list(filter(lambda point: type(point.scalar) is Scalar, self.points))

    def neighbors(self, point) -> List[Point]:
        return list(
            filter(
                lambda point: point,
                [
                    self._getPoint(point.x - 1, point.y),
                    self._getPoint(point.x + 1, point.y),
                    self._getPoint(point.x, point.y - 1),
                    self._getPoint(point.x, point.y + 1),
                ],
            )
        )

    def update_potential(self):
        i = 0
        scalars = self.scalars
        while (
            abs(max(map(lambda point: point.scalar.res, scalars))) > 0.01 and i < 3000
        ):
            print(i, max(map(lambda point: point.scalar.res, scalars)))
            for p, point in enumerate(scalars):
                neighbors = self.neighbors(point)
                if (
                    abs(point.scalar.res) < 0.01
                    and max(
                        map(
                            lambda point: point.scalar.res
                            if hasattr(point.scalar, "res")
                            else 0,
                            neighbors,
                        )
                    )
                    < 0.001
                ):
                    continue
                value = 0
                for neighbor in neighbors:
                    value += neighbor.scalar.value
                value /= 4
                point.scalar.res = (
                    value - point.scalar.value if point.scalar.value else 1
                )
                point.scalar.value += 1.9 * point.scalar.res
            i += 1
        print("iterations:", i)

    def update_vectors(self):
        for point in self.points:
            xneighbors = [
                self._getPoint(i, point.y) for i in [point.x - 1, point.x + 1]
            ]
            yneighbors = [
                self._getPoint(point.x, i) for i in [point.y - 1, point.y + 1]
            ]
            du_x = 0
            du_y = 0
            du_x += xneighbors[1].scalar.value if xneighbors[1] else point.scalar.value
            du_x -= xneighbors[0].scalar.value if xneighbors[0] else point.scalar.value
            du_x /= len([i for i in xneighbors if i])
            du_y += yneighbors[1].scalar.value if yneighbors[1] else point.scalar.value
            du_y -= yneighbors[0].scalar.value if yneighbors[0] else point.scalar.value
            du_y /= len([i for i in yneighbors if i])
            point.vector.x = du_x
            point.vector.y = -du_y

    def export(self) -> str:
        output = ""
        for point in self.points:
            output += f"{point.x} {point.y} {point.scalar.value}\n"
        return output

    def exportCSV(self) -> str:
        output = ""
        for row in self._grid:
            for cell in row:
                output += f"{cell.scalar.value};"
            output += "\n"
        return output

    def exportGraphic(self, filenames: List):
        data = []
        for point in self.points:
            data.append(
                {
                    "x": point.x,
                    "y": point.y,
                    "u": point.scalar.value,
                    "vx": point.vector.x,
                    "vy": point.vector.y,
                }
            )
        df = pd.DataFrame.from_dict(data)
        pivotted = df.pivot("x", "y", "u")
        fig = plt.pyplot.figure()
        seaborn.heatmap(pivotted)
        plt.pyplot.quiver(df["y"], df["x"], df["vy"], df["vx"])
        for filename in filenames:
            fig.savefig(filename)

    def __str__(self):
        output = ""
        for row in self._grid:
            line = ""
            for cell in row:
                line += f"{cell} "
            line += "\n"
            output += line
        return output
